#	Analysis of the Erasmus Friend Network in Padova

This repository contains code and papers written about the network of Erasmus students in Padova in 2018/19. The paper is divided into two parts (as needed for the Network Science course).
