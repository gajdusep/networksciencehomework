import os
import pandas as pd


class User:
    """
    User wrapper
    """
    def __init__(self, id, name, since_when):
        self.name = name
        self.id = id
        self.since_when = since_when


class Database:
    """
    Database class. Contains lists, dictionaries and sets for comfortable work with number of Users.
    When creating and object:
        >>> network = Network()
        >>> network.read_and_parse_user_list('some/path/to/database/file.txt')
    """
    def __init__(self):
        self.dict_by_name = {}
        self.dict_by_id = {}
        self.user_list = []
        self.user_set = {}

    def read_and_parse_user_list(self, path_to_file):
        self.user_list = []

        df = pd.read_csv(path_to_file, delimiter=';', converters={"ID":str})
        for index, row in df.iterrows():
            user = User(row['ID'], row['Name'], row['When'])
            self.user_list.append(user)

        for user in self.user_list:
            self.dict_by_id[user.id] = user
            self.dict_by_name[user.name] = user

        self.user_set = set(self.user_list)
