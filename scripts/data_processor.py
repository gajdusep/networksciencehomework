from data_classes import *
import matplotlib.pyplot as plt
from Network import Network
import numpy as np

network = Network()

# number of nodes
print('Number of nodes: ', network.number_of_nodes)

# number of links
print('Number of links: ', network.number_of_links)

network.compute_distribution()
print('Gamma: ', network.gamma)

# moments of the degree distribution
print('Average degree:    ', network.avg_degree.round(2))
print('Degree variance:   ', network.var_degree.round(2))
print('Median of degrees: ', network.median_degree.round(2))

# average distance and diameter
network.count_diameter_and_distances()
print('Diameter: ', network.diameter)
print('Average distance: ', network.avg_distance.round(2))
print('    Estimate for avg distance: <d> = ln(N)/ln(<k>) = ln(',
      network.number_of_nodes,  ') / ln(', network.avg_degree.round(2), ') =',
      (np.log(network.number_of_nodes)/np.log(network.avg_degree)).round(2))

# criticality
print('Network is in', network.criticality(), 'regime.')
print('    Because: <k>=', network.avg_degree, '; ln(N)=', np.log(network.number_of_nodes))

# clustering coefficient and its distribution
network.compute_clustering_coefficient()
print('Avg clustering coeff:', network.clustering_coeff_mean)

# assortativity exponent and nearest neighbours degree
network.assortativity()

# robustness to random failures and attacks, inhomogeneity ratio
network.compute_robustness()
print('Inhomogeneity ratio: ', network.inhomogeneity_ratio)

plt.tight_layout(pad=0.5, w_pad=0.5, h_pad=0.5)
plt.show()
