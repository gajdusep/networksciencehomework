import uuid
import hashlib
import pandas as pd
from Network import Network
import numpy as np


"""
HELPING FILE for hashing all the files and creating a file for gephi software

"""

def hash_string(s_to_hash, salt=''):
    return hashlib.md5(s_to_hash.encode() + salt.encode()).hexdigest()


def hash_database(database, salt, path_to_hashed):
    with open(path_to_hashed, "a", encoding='utf-8') as myfile:
        myfile.write('ID;Name;When\n')
        for user in database.user_list:
            s1 = hash_string(str(user.id), salt)
            s2 = hash_string(str(user.name), salt)
            s = s1 + ';' + s2 + ';***' + '\n'
            myfile.write(s)


def hash_network(salt, path_to_hashed):
    with open('../data_files/friends_file.txt', encoding='utf-8') as infile:
        with open(path_to_hashed, "a", encoding='utf-8') as myfile:
            for line in infile:
                splitted = line.split(';')
                curr_id = splitted[0]
                curr_name = splitted[1]
                curr_friends_set = eval(splitted[2])

                s1 = hash_string(curr_id, salt)
                s2 = hash_string(curr_name, salt)

                set2 = set()
                for fr in list(curr_friends_set):
                    set2.add(hash_string(fr, salt))
                s3 = repr(set2)

                s = s1 + ';' + s2 + ';' + s3 + '\n'
                myfile.write(s)


def hash_dephi(salt):
    dephi = '../data_files/for_dephi.csv'
    dephi_hash = '../data_files/for_dephi_hashed.csv'

    with open(dephi, encoding='utf-8') as infile:
        with open(dephi_hash, "a", encoding='utf-8') as myfile:
            for line in infile:
                splitted = line.split(';')
                s1 = hash_string(splitted[0], salt)
                s2 = hash_string(splitted[1], salt)


def save_for_dephi(network):
    indexes = np.transpose(np.transpose(np.nonzero(network.adjacency)))
    curr_users_names = network.group_df.columns.values
    dephi = '../data_files/for_dephi_hashed.csv'
    with open(dephi, "a", encoding='utf-8') as myfile:
        for i in range(0, indexes[0].shape[0]):
            first = curr_users_names[indexes[0][i]]
            second = curr_users_names[indexes[1][i]]

            first_name = network.database.dict_by_id[first].name
            second_name = network.database.dict_by_id[second].name
            s = '\"' + str(first_name) + '\";\"' + str(second_name) + '\"\n'
            myfile.write(s)


net = Network()
# save_for_dephi(net)

# salt = uuid.uuid4().hex

path_to_hashed = '../data_files/erasmus_users_hashed.txt'
# hash_database(net.database, salt, path_to_hashed)

path_to_hashed_network = '../data_files/friends_file_hashed.txt'
# hash_network(salt, path_to_hashed_network)



