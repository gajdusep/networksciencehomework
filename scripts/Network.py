import numpy as np
import matplotlib.pyplot as plt
from data_classes import Database, User
import pandas as pd
from scipy import sparse
from datetime import datetime
import os


class Network:
    """
    Wrapper for all networks properties and functions.
    """

    def __init__(self, ):
        self.group_df = None
        self.adjacency = None
        self.degrees_df = None
        self.degrees = None
        self.database = None
        self.unique_degrees = None

        self.number_of_nodes = 0
        self.number_of_links = 0
        self.avg_degree = 0
        self.var_degree = 0
        self.median_degree = 0
        self.gamma = 0
        self.diameter = 0
        self.avg_distance = 0
        self.inhomogeneity_ratio = 0
        self.clustering_coeff_mean = 0

        # (un)comment if you have real data or just the hashed ones...
        self.path_to_database_file = '../data_files/erasmus_users.txt'
        self.path_to_friends_lists_file = '../data_files/friends_file.txt'
        # self.path_to_database_file = '../data_files/erasmus_users_hashed.txt'
        # self.path_to_friends_lists_file = '../data_files/friends_file_hashed.txt'

        self.__plots_folder = '../data_files/plots/'

        # part 1 plots
        self.__spy_path = self.__plots_folder + 'spy.png'
        self.__deg_distribution_path = self.__plots_folder + 'deg_distribution.png'
        self.__clustering_path = self.__plots_folder + 'clustering.png'
        self.__assortativity_path = self.__plots_folder + 'assortativity.png'
        self.__robustness_path = self.__plots_folder + 'robustness.png'

        # part 2 plots
        self.__hits_pagerank_compare_path = self.__plots_folder + 'hits_pr.png'
        self.__sir_path = self.__plots_folder + 'sir'
        self.__laplac_eigs = self.__plots_folder + 'laplac_eigs.png'
        self.__conductance = self.__plots_folder + 'conductance.png'
        self.__gephi_path = '../data_files/gephi/'

        self.prepare_network(self.path_to_database_file, self.path_to_friends_lists_file)

    def prepare_network(self, path_to_database_file, path_to_friends_lists_file):
        """
        Method for reading all needed files and basic preparation of the dataframe and network variables.
        """

        # ---------- reading the database file and making network dataframe ----------
        self.database = Database()
        self.database.read_and_parse_user_list(path_to_database_file)

        size = len(self.database.user_list)
        df = pd.DataFrame(np.zeros((size, size), dtype='int64'))
        df.index = [x.id for x in self.database.user_list]
        df.columns = [x.id for x in self.database.user_list]

        """
        Process the file with lists of friends of erasmus users.
        ATTENTION: some data are not trustworthy. Mainly the following cases are bad:
            eg1) the user has "0" friends in common with erasmus -> 
                1) really has 0 friends (not so common), possible reasons: 
                    a) user will be on erasmus in the second semester, he doesn't know anyone yet
                    b) fake account
                2) user's privacy setting didn't allowed scraping...
            eg2) the user has suspiciously small amount of friends on Erasmus (2,3,4)
        SOLUTION:
            The data has to be transformed into matrix. 
            During creating this matrix, the friendships are being revealed 
                (user's private settings are not betrayed by his friend's settings) 
            The result matrix will be relevant.
        """
        with open(path_to_friends_lists_file, encoding='utf-8') as infile:
            for line in infile:
                splitted = line.split(';')
                curr_id = splitted[0]
                # curr_name = splitted[1]
                curr_friends_set = eval(splitted[2])
                for friend in curr_friends_set:
                    friends_id = self.database.dict_by_name[friend].id
                    df[curr_id][friends_id] = 1
                    df[friends_id][curr_id] = 1

        for i in range(0, size):
            df.values[i][i] = 0

        # ---------- spy the original data ----------
        """
        figure1, ax1 = plt.subplots(1, 2, sharex='none', sharey='none', squeeze=False)
        figure1.set_size_inches(8, 4)
        figure1.text(0.5, -0.07, 'Original data', horizontalalignment='center', transform=ax1[0, 0].transAxes)
        ax1[0, 0].spy(df.values, markersize=0.3)
        print('Number of nodes (orig data): ', df.values.shape[0])
        """

        # ---------- editing the dataset: remove nodes with irrelevant number of links ----------
        max_no_of_friends = 130
        new_df = df.loc[(df.sum(axis=1) < max_no_of_friends), (df.sum(axis=0) < max_no_of_friends)]
        # new_df = df  # COMMENT this line, if you WANT the upper bound
        new_df = new_df.loc[(new_df.sum(axis=1) != 0), (new_df.sum(axis=0) != 0)]

        self.group_df = new_df
        self.adjacency = self.group_df.values

        # ---------- spy the reduced data ----------
        """
        ax1[0, 1].spy(self.adjacency, markersize=0.3)
        figure1.text(0.5, -0.07, 'Reduced data', horizontalalignment='center', transform=ax1[0, 1].transAxes)
        figure1.savefig(self.__spy_path)
        """

        transpose = sum(sum(self.adjacency - np.transpose(self.adjacency))) == 0
        if not transpose:
            print('ERROR in adjacency matrix - it is asymetric!')
            exit()

        # ---------- basic network properties ----------
        self.number_of_nodes = self.group_df.shape[0]
        self.degrees_df = self.group_df.sum()
        self.degrees = self.degrees_df.values
        self.number_of_links = self.degrees.sum() // 2

        print('-------- Network built --------\n')

    def compute_distribution(self):
        """
        Method for counting the distribution parameters and plotting distribution plots.
        """

        figure1, ax1 = plt.subplots(1, 3, sharex=False, sharey=False, squeeze=False)

        # ---------- distribution
        if len(self.degrees[self.degrees == 0]) != 0:
            print('ERROR in degrees - some nodes have 0 neighbours')
            exit()

        self.unique_degrees, counts = np.unique(self.degrees, return_counts=True)
        counts_prob = counts / sum(counts)
        ax1[0, 0].plot(self.unique_degrees, counts, '.')
        ax1[0, 0].set_title('Degree distribution (log plot)')
        ax1[0, 0].set_xlabel('k')
        ax1[0, 0].set_yscale('log')
        ax1[0, 0].set_ylim(top=100)
        ax1[0, 0].set_ylabel('number of nodes with degree k')

        # ---------- cumulative distribution
        Pk = np.cumsum(counts_prob[::-1])[::-1]
        ax1[0, 1].set_title('Cumulative distribution (loglog plot)')
        ax1[0, 1].set_yscale('log')
        ax1[0, 1].set_xscale('log')
        ax1[0, 1].set_xlabel('k')
        ax1[0, 1].set_ylabel('P(k)')
        ax1[0, 1].plot(Pk, '.')

        # ---------- moments of the degree distribution
        self.avg_degree = self.number_of_links / self.number_of_nodes
        self.var_degree = self.degrees_df.var()
        self.median_degree = self.degrees_df.median()

        # ---------- estimate of the power law exponent
        kmin = 14
        degrees2 = self.degrees[self.degrees >= kmin]
        self.gamma = 1 + 1 / np.mean(np.log(degrees2 / kmin))
        s1 = np.power(self.unique_degrees, 1 - self.gamma)

        ax1[0, 2].set_title('ML estimate')
        ax1[0, 2].set_yscale('log')
        ax1[0, 2].set_xscale('log')
        ax1[0, 2].set_xlabel('k')
        ax1[0, 2].set_ylabel('P(k)')
        ax1[0, 2].plot(Pk, '.')
        mult = Pk[15]/s1[15]
        ax1[0, 2].plot(self.unique_degrees, s1 * mult)
        ax1[0, 2].text(0.1, 0.25, 'k_min=' + str(kmin) + '\ngamma=' + str(self.gamma.round(2)),
                       fontsize=12, transform=ax1[0, 2].transAxes,
                       verticalalignment='top', horizontalalignment='left')
        figure1.set_size_inches(12, 4)
        plt.tight_layout(pad=0.5, w_pad=0.5, h_pad=0.5)

        # figure1.savefig(self.__deg_distribution_path)

    def criticality(self):
        """
        Counts criticality
        :return: string with type of network phase
        """

        if self.avg_degree < 1:
            return 'subcritical'
        elif self.avg_degree == 1:
            return 'critical'
        elif self.avg_degree < np.log(self.number_of_nodes):
            return 'supercritical'
        else:
            return 'connected'

    def compute_clustering_coefficient(self):
        """
        Computes clustering coefficient and saves plots.
        """

        # ---------- clustering coefficient for all nodes ----------
        #    - c_i = (2*E_i) / (k_i * (k_i - 1)) where
        #        - k_i = number of neighbours of node i
        #        - E_i = number of edges between the neighbours of the node i
        clustering_coeff = np.zeros(self.number_of_nodes)
        for i in range(0, self.number_of_nodes):  # for every node
            neighbours_of_i = np.where(self.adjacency[i] > 0)[0]
            neighbours_connected = 0

            for j in neighbours_of_i:  # sum the number of friendships of friends
                for k in neighbours_of_i:
                    if j != k:
                        if self.adjacency[j, k] != 0:
                            neighbours_connected = neighbours_connected + 1

            neighbours_connected = 0.5 * neighbours_connected
            e_max = 0.5 * self.degrees[i] * (self.degrees[i] - 1)
            clustering_coeff[i] = neighbours_connected / e_max if e_max != 0 else 0

        self.clustering_coeff_mean = np.mean(clustering_coeff)

        # ---------- ploting the clustering coefficient ----------
        figure, ax = plt.subplots(1, 1, sharex='none', sharey='none', squeeze=False)
        text_str = 'Average clustering coeff = ' + str(self.clustering_coeff_mean.round(2))
        ax[0, 0].set_xscale('log')
        ax[0, 0].set_yscale('log')
        ax[0, 0].set_xlabel('k')
        ax[0, 0].set_ylabel('cc_k')
        ax[0, 0].set_title('Clustering coeff')
        ax[0, 0].axhline(y=self.clustering_coeff_mean, color='g')
        ax[0, 0].plot(self.degrees, clustering_coeff, '.')
        ax[0, 0].text(0.1, 0.1, text_str, transform=ax[0, 0].transAxes,
                      verticalalignment='top', horizontalalignment='left')
        figure.set_size_inches(5, 5)
        figure.savefig(self.__clustering_path)

    def assortativity(self):
        """
        Method for computing the assortativity of the network and plotting the results.
        """

        # ---------- degree correlation matrix ----------
        unique_deg_size = 50
        heat_map_size = unique_deg_size

        heat_map = np.zeros((heat_map_size, heat_map_size))
        nonzero_indeces = np.transpose(np.nonzero(self.adjacency))
        ll = heat_map.shape[0]
        for pair in nonzero_indeces:
            id1 = self.degrees[pair[0]]
            id2 = self.degrees[pair[1]]
            if id1 < ll and id2 < ll:
                heat_map[id1, id2] += 1

        how_much = 2
        heat_map2 = np.zeros((heat_map_size, heat_map_size))
        for i in range(how_much, ll - how_much):
            for j in range(how_much, ll - how_much):
                heat_map2[i, j] = np.mean(heat_map[i-how_much:i+how_much,j-how_much:j+how_much])
        heat_map2 = heat_map2 / heat_map2.sum().sum()

        # ---------- plotting the degree correlation matrix ----------
        figure, ax = plt.subplots(1, 2, sharex='none', sharey='none', squeeze=False)
        img = ax[0, 0].imshow(heat_map2, cmap='GnBu')
        figure.colorbar(img, ax=ax[0, 0])
        ax[0, 0].set_xlabel('k')
        ax[0, 0].set_ylabel('k')
        ax[0, 0].set_title('Degree correlation matrix - max degree = ' + str(unique_deg_size))
        ax[0, 0].invert_yaxis()

        # ---------- nearest neighbour degree ----------
        k_tmp = np.divide(np.dot(self.adjacency, self.degrees.reshape(-1, 1)), self.degrees.reshape(-1, 1))
        k_nn = np.zeros(len(self.unique_degrees))
        for i in range(0, len(self.unique_degrees)):
            k_nn[i] = np.mean(k_tmp[self.degrees == self.unique_degrees[i]])

        # ---------- computation of assortativity factor ----------
        p = np.polyfit(np.log(self.unique_degrees), np.log(k_nn), 1)
        assortativity_factor = p[0]

        # ---------- plotting the assortativity plot ----------
        text_str = 'Assortativity factor = ' + str(assortativity_factor.round(2))
        ax[0, 1].set_xscale('log')
        ax[0, 1].set_yscale('log')
        ax[0, 1].plot(self.degrees, k_tmp, '.')
        ax[0, 1].plot(self.unique_degrees, k_nn, 'r.')
        ax[0, 1].plot(self.unique_degrees, np.exp(p[1] + np.log(self.unique_degrees) * p[0]))
        ax[0, 1].set_xlabel('k')
        ax[0, 1].set_ylabel('k_nn')
        ax[0, 1].set_title('Assortativity')
        ax[0, 1].text(0.4, 0.1, text_str, transform=ax[0, 1].transAxes,
                      verticalalignment='top', horizontalalignment='left')
        figure.set_size_inches(10, 4)
        figure.tight_layout()
        figure.savefig(self.__assortativity_path)

    @staticmethod
    def count_inhomogeneity_ratio(adjacency_matrix):
        """
        Count inhomogeneity ratio of given matrix
        :param adjacency_matrix: numpy array, adj matrix of the network
        """

        deg = np.sum(adjacency_matrix, axis=0)
        deg_2 = np.power(deg, 2)

        if np.mean(deg) == 0:
            return 0
        kappa = np.mean(deg_2) / np.mean(deg)
        return kappa

    @staticmethod
    def robustness(robustness_df, attack):
        """
        :param robustness_df: network dataframe
        :param attack: True-attack, False-random failure
        :return: np array, sizes of the biggest component during the attack/failure
        """

        n = robustness_df.shape[0]
        giant_component_sizes = []
        breaking_point = np.inf

        # remove nodes one by one
        for i in range(0, n - 1):
            robustness_adj = robustness_df.values
            inhom_ratio = Network.count_inhomogeneity_ratio(robustness_adj)

            # if inhom ratio not saved yet and is bellow 2, save it
            if inhom_ratio < 2:
                if i < breaking_point:
                    breaking_point = i

            sparse_adj = sparse.csr_matrix(robustness_adj)
            n_components, labels = sparse.csgraph.connected_components(sparse_adj, directed=False, return_labels=True)
            labels_unique = np.unique(labels)
            giant_comp = np.histogram(labels, labels_unique)[0].max()
            giant_component_sizes.append(giant_comp / n)

            if attack:
                idx = robustness_df.sum(axis=1).sort_values(ascending=False).head(1)
                index_to_remove = idx.index[0]
            else:
                idx = robustness_df.sample(n=1)
                index_to_remove = idx.index[0]

            robustness_df = robustness_df.drop(index_to_remove)
            robustness_df = robustness_df.drop(index_to_remove, axis=1)

        return np.array(giant_component_sizes), breaking_point / n

    def compute_robustness(self):
        """
        Method for saving the plots of the graph under attack and random failure.
        """

        robustness_df = self.group_df
        kappa = Network.count_inhomogeneity_ratio(robustness_df)
        self.inhomogeneity_ratio = kappa

        with_attack, br_point_attack = Network.robustness(robustness_df, attack=True)
        without_attack, br_point_failure = Network.robustness(robustness_df, attack=False)
        print('attack br point', br_point_attack)
        print('failure br point', br_point_failure)
        print('breaking point according to the equation: ', str(1 - 1 / (kappa - 1)))

        n = robustness_df.values.shape[0] - 1
        figure, ax = plt.subplots(1, 1, sharex='none', sharey='none', squeeze=False)
        ax[0, 0].plot(np.linspace(0, n, n)/n, with_attack, 'b.', label='attack')
        ax[0, 0].plot(np.linspace(0, n, n)/n, without_attack, 'r.', label='random failure')
        ax[0, 0].set_xlabel('f')
        ax[0, 0].set_ylabel('P(f) / P(0)')
        ax[0, 0].legend()

        figure.set_size_inches(5, 5)
        figure.savefig(self.__robustness_path)

    def count_diameter_and_distances(self):
        """
        Count and save the diameter and average distance.
        """

        dist_matrix = sparse.csgraph.floyd_warshall(self.adjacency)
        non_inf = dist_matrix[dist_matrix < np.inf]
        non_inf = non_inf[non_inf != 0]
        un, co = np.unique(non_inf, return_counts=True)

        self.diameter = un[::-1][0]
        self.avg_distance = np.mean(non_inf)

    """
    ------------------------------
    Part 2: ranking, communities, link prediction, disease spreading
    ------------------------------    
    """

    """ Ranking methods """
    def ranking(self):
        # ---------------- HITS ----------------
        s = datetime.now()
        p1 = np.real(self.hits_eigenvalues())
        f = datetime.now()
        print('HITS algorithm - eigenvalues: ', (f - s).microseconds)

        s = datetime.now()
        iters = 30
        p2 = np.real(self.hits_iterations(iters=iters))
        f = datetime.now()
        print('HITS algorithm - iterations: ', (f - s).microseconds)

        ind = p2.argsort()[::-1]
        sss = 0
        for i in range(0, 10):
            sss += self.degrees[ind[i]]
        print(sss / 10)

        # ---------------- PageRank ----------------
        s = datetime.now()
        p3 = self.page_rank_linear_system()
        f = datetime.now()
        print('PageRank algorithm - linear system: ', (f - s).microseconds)

        s = datetime.now()
        p4 = self.page_rank_iterations()
        f = datetime.now()
        print('PageRank algorithm - iterations: ', (f - s).microseconds)

        ind = p4.argsort()[::-1]
        sss = 0
        for i in range(0, 10):
            sss += self.degrees[ind[i]]
        print(sss / 10)

        # Check how similar are the results from HITS and PageRank
        fig, ax = plt.subplots()
        ax.plot(p1, p3, '.b')
        ax.set_title('Comparison of HITS and PageRank values')
        ax.set_xlabel('HITS')
        ax.set_ylabel('PageRank')
        fig.savefig(self.__hits_pagerank_compare_path)

    def hits_eigenvalues(self):
        A = self.adjacency
        M = np.dot(np.transpose(A), A)
        eig_vals, eig_vecs = np.linalg.eig(M)
        eig_vec1 = eig_vecs[:, 0]
        p = -eig_vec1 / np.linalg.norm(eig_vec1)

        # scores = np.argsort(p)
        # for i in range(0, 20):
        #     print(self.database.dict_by_id[self.group_df.columns[scores[i]]].name)

        return p

    def hits_iterations(self, iters=30):
        adjacency = self.adjacency
        M = np.dot(adjacency, np.transpose(adjacency))
        n = M.shape[0]
        p0 = np.ones(n) / np.sqrt(n)
        s = np.zeros(iters)

        for i in range(0, iters):
            p00 = p0
            p0 = np.dot(adjacency, np.dot(np.transpose(adjacency), p0))
            p0 = p0 / np.linalg.norm(p0)
            s[i] = np.linalg.norm(p0 - p00) / np.sqrt(n)

        # scores = np.argsort(p0)[::-1]
        # for i in range(0, 20):
        #     print(self.database.dict_by_id[self.group_df.columns[scores[i]]].name)

        return p0

    def page_rank_iterations(self):
        A = self.adjacency
        n = A.shape[0]
        M = np.dot(A, np.diag(1.0 / sum(A)))
        c = 0.85
        q = np.ones(n) / n

        p0 = np.ones(n) / n
        number_of_iters = 30
        s = np.zeros(number_of_iters)

        for i in range(0, number_of_iters):
            p00 = p0
            p0 = c * np.dot(M, p0) + (1 - c) * q
            p0 = p0 / sum(p0)

        # scores = np.argsort(p0)[::-1]
        # for i in range(0, 20):
        #     print(network.database.dict_by_id[self.group_df.columns[scores[i]]].name)

        return p0

    def page_rank_linear_system(self):
        A = self.adjacency
        n = A.shape[0]
        M = np.dot(A, np.diag(1.0 / sum(A)))
        c = 0.85
        q = np.ones(n) / n

        r = np.linalg.solve((np.eye(n) - c * M) / (1 - c), q)
        r = r / sum(r)

        # scores = np.argsort(r)[::-1]
        # for i in range(0, 20):
        #     print(network.database.dict_by_id[self.group_df.columns[scores[i]]].name)

        return r

    """ Link prediction methods """

    @staticmethod
    def link_prediction_neighbor(a, **kwargs):
        """
        :param a: numpy adjacency matrix
        :param algo: 'common_neighbours'/'adamic-adar'/'resource-allocation'
        :return: matrix with evaluated edges
        """

        algo = 'common_neighbours'
        if 'algo' in kwargs:
            algo = kwargs['algo']

        if algo == 'common_neighbours':
            s_cn = np.dot(a, a.transpose())
            return s_cn

        degs = np.sum(a, axis=0)

        n = a.shape[0]
        s_aa = np.zeros(a.shape)
        s_ra = np.zeros(a.shape)

        for i in range(0, n):
            for j in range(i, n):
                # neighbours of i and j:
                neig_i = a[i]
                neig_j = a[j]

                # common neighbours:
                indeces_of_intersect = np.where(neig_i + neig_j == 2)[0]

                # compute the 'probability' of edge (i,j)
                for k in indeces_of_intersect:
                    d = degs[k]
                    if d != 0:
                        s_ra[i, j] += 1 / d
                    if d != 1:
                        s_aa[i, j] += 1 / np.log(d)

        if algo == 'adamic_adar':
            return s_aa

        if algo == 'resource_allocation':
            return s_ra

        raise ValueError("Algorithm unknown. Choose: 'common_neighbours', 'adamic_adar', 'resource_allocation'.")

    @staticmethod
    def link_prediction_path(a, **kwargs):
        """
        Link prediction based on path.
        :param a: numpy adjacency matrix
        :param algo: 'kats'/'local_path'
        :param kwargs: beta
        :return: matrix with evaluated edges
        """

        beta = 0.3
        max_l = 8
        algo = 'kats'

        if 'algo' in kwargs:
            algo = kwargs['algo']
        if 'beta' in kwargs:
            beta = kwargs['beta']
        if 'max_l' in kwargs:
            max_l = kwargs['max_l']

        # Kats algorithm
        if algo == 'kats':
            s_katz = np.zeros(a.shape)
            for i in range(1, max_l):
                s_katz = s_katz + np.power(beta, i) * np.linalg.matrix_power(a, i)
            return s_katz

        # local path:
        if algo == 'local_path':
            s_lp = np.dot(a, a) + beta * np.linalg.matrix_power(a, 3)
            return s_lp

        raise ValueError("Algorithm unknown. Choose: 'kats' or 'local_path'")

    def prediction_goodness(self, prediction_method, **kwargs):
        A = self.adjacency
        n = A.shape[0]
        triang = np.triu(A)

        # get list of indexes of non zero elements (edges)
        non_zero = np.nonzero(triang)
        zero = np.where(triang == 0)
        indexes = np.array([non_zero[0], non_zero[1]]).transpose()
        np.random.seed(2)
        np.random.shuffle(indexes)

        # following lines divide set into test and size
        #     must take into account the symmetric matrix and not count each edge twice
        test_size = indexes.shape[0] // 10
        train_size = indexes.shape[0] - test_size

        training_indexes_triang = indexes[:train_size, :]
        testing_indexes_triang = indexes[train_size:, :]
        inactive_indexes_triang = np.array([zero[0], zero[1]]).transpose()
        inactive_indexes_triang = np.array([[x, y] for [x, y] in inactive_indexes_triang if x < y])

        t2 = training_indexes_triang.transpose()[[1, 0]].transpose()
        tt2 = testing_indexes_triang.transpose()[[1, 0]].transpose()

        training_indexes = np.append(training_indexes_triang, t2, axis=0)
        testing_indexes = np.append(testing_indexes_triang, tt2, axis=0)

        # make the adjacency matrix only with training edges
        training_matrix = np.zeros((n, n))
        training_matrix[training_indexes[:, 0], training_indexes[:, 1]] = 1

        # GET THE SCORE MATRIX
        score_matrix = prediction_method(training_matrix, **kwargs)

        # asign -1 to already friends
        score_matrix[training_indexes[:, 0], training_indexes[:, 1]] = -1

        algo = 'unknown'
        if 'algo' in kwargs:
            algo = kwargs['algo']
        del kwargs['algo']

        # check the prediction goodness
        print(algo, 'with parameters: ', kwargs)
        result = Network.precision_test(score_matrix, test_size, testing_indexes_triang)
        print('    precision:', np.round(result, 4))
        result = Network.auc_test(score_matrix, testing_indexes_triang, inactive_indexes_triang)
        print('    auc:      ', np.round(result, 4))

    @staticmethod
    def precision_test(score_matrix, k, testing_indexes_triang):

        # from score matrix get the lists of sorted indexes
        ss = np.unravel_index(np.argsort(score_matrix, axis=None), score_matrix.shape)
        result_indexes = np.array([ss[0][::-1], ss[1][::-1]]).transpose()
        result_indexes = np.array([[x, y] for [x, y] in result_indexes if x < y])[:k]

        # create sets for intersection, precision
        result_set = set(list(map(tuple, result_indexes)))
        testing_set = set(list(map(tuple, testing_indexes_triang)))

        inters = len(result_set.intersection(testing_set))
        # print('whole len:', test_size, '=', len(result_set), '=', len(testing_set))
        return inters / k

    @staticmethod
    def auc_test(score_matrix, test_set_indexes, inactive_set_indexes):
        auc = 0
        no_inactive = len(inactive_set_indexes)

        # always take n random inactive edges
        random_n = 50
        for (x_t, y_t) in test_set_indexes:
            perm = np.random.permutation(no_inactive-1)
            for j in range(0, random_n):
                (x_i, y_i) = inactive_set_indexes[j]
                if score_matrix[x_t, y_t] > score_matrix[x_i, y_i]:
                    auc = auc + 1
        auc = auc / (len(test_set_indexes)*random_n)
        return auc

    """ Community detection methods """

    def modularity_clustering(self):
        print('-------- Modularity ----------')

        a = np.copy(self.adjacency)
        # remove all that is not in the giant component
        sparse_adj = sparse.csr_matrix(a)
        n_components, labels = sparse.csgraph.connected_components(sparse_adj, directed=False, return_labels=True)
        inddd = [labels == 0][0]
        a = a[inddd]
        a = a[:, inddd]

        # make first two clusters
        labels = Network.modularity_optimization_two_clusters(a)
        # print(len(labels[labels == 1]))
        # print(len(labels[labels == 2]))

        Q = Network.compute_split_modularity(a, labels)
        print('first Q: ', np.round(Q, 4))

        last_index = 3
        epsilon = 0.01

        for j in range(0, 2):
            i = 0
            # call "recursively" on newly made clusters
            while i <= last_index:
                i += 1
                ind = labels == i
                smaller_a = a[:, ind.flatten()]
                smaller_a = smaller_a[ind.flatten(), :]

                # make two new clusters
                labels2 = Network.modularity_optimization_two_clusters(smaller_a)
                # print(len(labels2[labels2 == 1]))
                # print(len(labels2[labels2 == 2]))

                labels2[labels2 == 2] = i + 1 #last_index
                labels2[labels2 == 1] = i

                # try to improve the modularity by adding the new made cluster
                l = np.copy(labels)
                l[l > i] = l[l > i] + 1
                l[ind] = labels2.flatten()
                maybe_better_q = Network.compute_split_modularity(a, l)

                # if the modularity improved or didn't get MUCH worse (epsilon), add new cluster
                if maybe_better_q > Q - epsilon and maybe_better_q != Q and len(np.unique(labels2)) != 1:
                    Q = maybe_better_q
                    labels[labels > i] = labels[labels > i] + 1
                    labels[ind] = labels2.flatten()
                    last_index += 1

        # print(labels.flatten())
        unique, counts = np.unique(labels, return_counts=True)
        print('labels and its sizes', dict(zip(unique.astype(int), counts)))
        print('final Q', np.round(Q, 4))
        print('-------------------')

        edg = self.__gephi_path + 'modularity_edges.csv'
        nod = self.__gephi_path + 'modularity_nodes.csv'
        Network.write_labels(labels, a, edg, nod)

        return a, labels

    @staticmethod
    def compute_split_modularity(a, labels):
        """
        Compute the modularity according to the modularity equation.
        :param a: Adjacency matrix
        :param labels: Labels of nodes
        :return: Modularity value
        """

        l = np.sum(np.sum(a))
        n = a.shape[0]
        deg = np.sum(a, axis=0).reshape(-1, 1)
        k_mat = np.dot(deg, deg.transpose())
        B = a - k_mat / (2 * l)

        squared = np.dot(labels, labels.transpose())
        powered_2 = np.repeat(np.power(labels, 2), n, axis=1)
        ind = squared == powered_2
        eta = np.zeros((n, n))
        eta[ind] = 1

        return (1 / (2 * l)) * sum(sum(np.multiply(B, eta)))

    @staticmethod
    def modularity_optimization_two_clusters(a):
        """
        Get the labels of given adjacency matrix.
        :param a: adjacency matrix
        :return: labels of nodes, either 1 or 2. Also might return only ones, if it can't be divided into groups
        with number of  members > 20
        """

        n = a.shape[0]

        # don't accept clusters with size < min_members
        min_members = 20
        if n < min_members:
            return np.ones((n, 1))

        deg = np.sum(a, axis=0).reshape(-1, 1)
        l = np.sum(np.sum(a))
        k_mat = np.dot(deg, deg.transpose())
        B = a - k_mat / (2 * l)

        # leading eigenvector
        vals, vecs = np.linalg.eig(B)
        idx = vals.argsort()[::-1]
        vecs = vecs[:, idx]

        # sign of leading eigenvector - first initiative
        s = vecs[:, 0]
        s = np.sign(np.real(s))

        # try to improve the cluster division
        Q = np.linalg.multi_dot([s.transpose(), B, s]) / (4 * l)
        iters = 5
        for i in range(0, iters):
            for j in range(0, len(s)):
                s[j] = -s[j]
                new_Q = np.linalg.multi_dot([s.transpose(), B, s]) / (4 * l)
                if new_Q > Q:
                    Q = new_Q
                else:
                    s[j] = -s[j]

        # check that both clusters have more than min_members
        if len(s[s == -1]) < min_members or len(s[s == 1]) < min_members:
            return np.ones((n, 1))

        # else return the division made by modularity optimization
        labels = np.zeros((n, 1))
        labels[s == -1] = 1
        labels[s == 1] = 2

        return labels

    @staticmethod
    def write_labels(labels, a, edges_path, nodes_path):
        """
        Write the labels into two files for gephi. Makes two files: edges and nodes.
        """

        if os.path.exists(edges_path):
            os.remove(edges_path)
        if os.path.exists(nodes_path):
            os.remove(nodes_path)

        f = open(edges_path, "w+")
        # f.write("Source;Target\n")
        s = a.shape[0]
        for i in range(0, s):
            for j in range(0, s):
                if a[i, j] == 1:
                    sss = '"' + str(i) + '";"' + str(j) + '"\n'
                    f.write(sss)
        f.close()

        f = open(nodes_path, "w+")
        s = a.shape[0]
        f.write("Id;Attribute\n")
        for i in range(0, s):
            sss = '"' + str(i) + '";"' + str(np.int(labels[i][0])) + '"\n'
            f.write(sss)
        f.close()

    def spectral_clustering(self):
        """
        Runs the whole spectral clustering method.
        :return: Adjacency matrix (the original one might be reduced), labels
        """

        A = np.copy(self.adjacency)

        # remove all that is not in the giant component
        sparse_adj = sparse.csr_matrix(A)
        n_components, labels = sparse.csgraph.connected_components(sparse_adj, directed=False, return_labels=True)
        inddd = [labels == 0][0]
        A = A[inddd]
        A = A[:, inddd]

        # make normalized Laplacian
        n = A.shape[0]  # number of nodes
        Di = np.diag(1 / np.sqrt(np.sum(A, axis=1)))  # diagonal matrix D, on diagonal: degree^(-0.5)
        L = np.diag(np.ones(n)) - np.dot(Di, np.dot(A, Di))  # Normalized Laplacian

        # obtain eigenvalues
        eig_vals, eig_vecs = np.linalg.eig(L)
        sorted_ind = eig_vals.argsort()  # np.linalg.eig(L) doesn't return sorted eigenvalues
        sorted_eig_vec = eig_vecs[:, sorted_ind]
        sorted_eig_val = eig_vals[sorted_ind]

        # plot eigenvalues
        fig, ax = plt.subplots()
        ax.set_title('Eigenvalues of normalized Laplacian')
        ax.plot(sorted_eig_val, '.')
        fig.savefig(self.__laplac_eigs)

        vv = np.dot(Di, sorted_eig_vec)

        v1 = vv[:, 1] / np.linalg.norm(vv[:, 1])  # Fiedler's vector
        v2 = vv[:, 2] / np.linalg.norm(vv[:, 2])  # other eigenvectors can be used to further division of communities

        # cound the biggest eigengap - not used, might be finished in the future
        biggest_gap = -1
        index_biggest_gap = -1
        for i in range(len(sorted_eig_val) - 20, len(sorted_eig_val) - 2):
            if sorted_eig_val[i] - sorted_eig_val[i + 1] > biggest_gap:
                biggest_gap = sorted_eig_val[i] - sorted_eig_val[i + 1]
                index_biggest_gap = i
        no_of_eigs = len(sorted_eig_val) - index_biggest_gap

        # sweep
        vv_sorted_ind = np.argsort(v1)
        AA = A[vv_sorted_ind]
        AA = AA[:, vv_sorted_ind]

        # conductance measure
        a = np.sum(np.triu(AA), axis=0)
        b = np.sum(np.tril(AA), axis=0)
        d = a + b
        D = np.sum(d)
        assoc = np.cumsum(d)
        D_assoc = np.repeat(D, len(assoc)) - assoc
        assoc = np.minimum(assoc, D_assoc)
        cut = np.cumsum(b - a)
        conductance = cut / assoc
        conductance = conductance[:-1]

        divide_line = np.argmin(conductance)
        print('Network divided into two communities:', divide_line, n - divide_line)
        print('Min conductance:', np.round(min(conductance), 4))
        fig, ax = plt.subplots()
        ax.set_xlabel('node number')
        ax.set_ylabel('conductance')
        ax.set_title('Conductance')
        ax.plot(conductance)
        fig.savefig(self.__conductance)

        labels = np.zeros((a.shape[0], 1))
        labels[vv_sorted_ind[:divide_line]] = 1
        labels[vv_sorted_ind[divide_line:]] = 2

        edg = self.__gephi_path + 'spectral_edges.csv'
        nod = self.__gephi_path + 'spectral_nodes.csv'
        Network.write_labels(labels, A, edges_path=edg, nodes_path=nod)

        return A, labels

    """ Disease simulation methods """

    def sir_simulation(self, beta=0.1, mi=0.3, iters=60, nodes_to_start=[], plot_no=1):
        a = self.adjacency
        n = self.number_of_nodes

        no_of_start_nodes = 3
        nodes_to_start = [nodes_to_start] if nodes_to_start != [] else [np.random.permutation(n)[:no_of_start_nodes]]

        # 0 - sane, 1 - infected, 2 - recovered
        health = np.zeros(n)
        health[tuple(nodes_to_start)] = 1

        # set the result vectors
        to_show_ill = np.zeros(iters)
        to_show_healthy = np.zeros(iters)
        to_show_recovered = np.zeros(iters)

        for i in range(0, iters):
            # sanitize
            # simulate random sanitizing
            rnd_vec = np.random.rand(n)
            for j in range(0, n):
                if health[j] == 1 and rnd_vec[j] < mi:
                    health[j] = 2

            # make ill
            infected_ind = health == 1
            neighbors_of_infected_ind = a[infected_ind].sum(axis=0) > 0  # take all neighbours of ill
            non_infected_neighs = np.bitwise_and(neighbors_of_infected_ind, health == 0)  # choose just sane ones
            # simulate random catching disease
            rnd_vec = np.random.rand(n)
            for j in range(0, n):
                if non_infected_neighs[j] and rnd_vec[j] < beta:
                    health[j] = 1

            # save the actual state of population
            to_show_healthy[i] = health[health == 0].shape[0]
            to_show_ill[i] = health[health == 1].shape[0]
            to_show_recovered[i] = health[health == 2].shape[0]

        fig, ax = plt.subplots()
        ax.set_xticks(np.arange(0, iters, step=10))
        ax.set_title('SIR: beta=' + str(beta) + ', mi=' + str(mi) + '')
        ax.plot(to_show_recovered, '-b.', label='Recovered')
        ax.plot(to_show_ill, '-r.', label='Infected')
        ax.plot(to_show_healthy, '-g.', label='Healthy')
        ax.legend()
        fig.savefig(self.__sir_path+str(plot_no))
