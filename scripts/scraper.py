# !!! WATCH OUT !!!
# TODO: always delete password when commiting and pushing
# !!! WATCH OUT !!!

import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from data_classes import *
import time
from selenium.webdriver.common.keys import Keys


def get_set_of_friends_names(driver, user_id):
    """
    Method for getting the set of user's friends
    :param driver: selenium driver
    :param user_id: user's ID
    :return: set of friends
    """

    driver.get(facebook + user_id)
    possible_friends_link = driver.find_elements_by_xpath("//*[text()='Friends']")

    # the case that no friends are visible
    if len(possible_friends_link) == 1:
        return []
    try:
        possible_friends_link[1].click()
    except Exception:
        print("something wrong with clicking")
        return []

    list_of_friends = []
    while True and len(list_of_friends) <= 1200:
        images = driver.find_elements_by_tag_name("img")
        names = [x.get_attribute("alt") for x in images]

        list_of_friends = list_of_friends + names
        print("len: ", len(list_of_friends))
        # break
        try:
            block = driver.find_element_by_id("m_more_friends")
            block.find_element_by_tag_name("a").click()
        except Exception:
            break

    list_of_friends = [x for x in list_of_friends if x != '']
    print('number of (available) friends', len(list_of_friends))
    print()
    return set(list_of_friends)

# create a new driver
chrome_options = Options()
chrome_options.add_experimental_option("prefs", {'profile.managed_default_content_settings.javascript': 2})
chrome_driver_path = os.getcwd() + "/../drivers/chromedriver.exe"
driver = webdriver.Chrome(chrome_driver_path, chrome_options=chrome_options)

# set you username and password, NEVER PUSH IT ON GIT!!!
facebook = "https://mobile.facebook.com/"
username = "your_mail@domain.com"
password = "somerandompassword"

# start the driver
driver.get(facebook)
driver.find_element_by_name("email").send_keys(username)
driver.find_element_by_name("pass").send_keys(password)
driver.find_element_by_name("login").click()

database = Database()
database.read_and_parse_user_list("erasmus_users.txt")

# set the borders of checking - the scraping takes a long time, you might want to limit it (50 users =.= 10 minutes)
short_list = [x for x in database.user_list][0:0]

print()

name_of_file_with_friends = "friends_file.txt"
group_names_set = set([x.name for x in database.user_list])

start = time.time()
print('measuring begun')
act = 0
lll = len(short_list)
with open(name_of_file_with_friends, "a", encoding='utf-8') as myfile:
    for user in short_list:
        print('-------------', act, '/', lll, '----------------')
        act = act + 1
        print(user.name)
        friends_set = get_set_of_friends_names(driver, user.id)
        relevant_friends = group_names_set.intersection(friends_set)
        print("common friends with erasmus group: ", len(relevant_friends))
        print(relevant_friends)
        myfile.write(str(user.id + ";" + str(user.name) + ";" + repr(relevant_friends)))
        myfile.write("\n")

end = time.time()
print('the whole procedure was this long: ', (end-start)/60, 'minutes')
