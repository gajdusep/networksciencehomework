from data_classes import *
from Network import Network


net = Network()

print("--------Ranking--------")
net.ranking()

print("--------Community detection--------")
a, labels = net.modularity_clustering()
net.spectral_clustering()

print("--------Link prediction--------")
net.prediction_goodness(Network.link_prediction_neighbor, algo='common_neighbours')
net.prediction_goodness(Network.link_prediction_neighbor, algo='adamic_adar')
net.prediction_goodness(Network.link_prediction_neighbor, algo='resource_allocation')
net.prediction_goodness(Network.link_prediction_path, algo='local_path', beta=0.15)
net.prediction_goodness(Network.link_prediction_path, algo='local_path', beta=0.01)
net.prediction_goodness(Network.link_prediction_path, algo='kats', beta=0.01)
net.prediction_goodness(Network.link_prediction_path, algo='kats', beta=0.001)

print("--------Disease spreading--------")
net.sir_simulation(nodes_to_start=[762, 608], beta=0.1, mi=0.3, plot_no=1)
net.sir_simulation(nodes_to_start=[762, 608], beta=0.3, mi=0.1, plot_no=2)
net.sir_simulation(nodes_to_start=[762, 608], beta=0.5, mi=0.5, plot_no=3)
net.sir_simulation(nodes_to_start=[762, 608], beta=0.125, mi=0.1, plot_no=4)
